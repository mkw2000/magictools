import Vue from 'vue'
import VueFire from 'vuefire'
import './plugins/vuetify'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from './components/Home'
import Game from './components/Game'
import Profile from './components/Profile'
import Leaderboard from './components/Leaderboard'


Vue.use(VueFire)
Vue.use(VueRouter)


Vue.config.productionTip = false

const routes = [
    { path: '/', component: Home },
    { path: '/#/game', component: Game },
    { path: '/#/profile', component: Profile },
    { path: '/#/leaderboard', component: Leaderboard }

]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes // short for `routes: routes`
})


new Vue({
    render: h => h(App),
    router
}).$mount('#app')